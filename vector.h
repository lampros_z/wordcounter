#include <stddef.h>
#ifndef _VECTOR_H
#define _VECTOR_H
    typedef struct _vector* vector;
    typedef char value_type;

    vector vector_create();
    void vector_destroy(vector v);
    value_type vector_get(vector v, int position);
    void vector_put(vector v, int position, value_type value);
    void vector_add(vector v, value_type value);
    void vector_add_at(vector v, int position, value_type value);
    value_type vector_remove_at(vector v, int position);
    int vector_is_empty(vector v);
    size_t vector_size(vector v);
    void print_vector(vector v);
    void vector_clear(vector v);
#endif
