#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <dirent.h>
#include <sys/wait.h>

#define MAX_PATH_LENGTH  255
#define DEFAULT_PATH "."
#define CHARACTERS_PER_THREAD 50

bool is_directory(char* path, bool suppress_error_messages);
bool is_file(char *path);
bool change_directory(char* path);
bool is_ascii_file(char* filepath);
bool can_open(char* filepath);
char* ltoa(long value);

void signal_handler(int sig);

int main(int argc, char** argv) {
    signal(SIGINT,  signal_handler);
    signal(SIGTERM, signal_handler);
    long  characters_per_thread = CHARACTERS_PER_THREAD;
    char* path = (char *)calloc(MAX_PATH_LENGTH, sizeof(char));
    strcpy(path, DEFAULT_PATH);


    if( argc > 3 ){
        fprintf(stderr, "Usage: %s <directory_path> <thread_characters>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if(argc == 2){
        characters_per_thread = strtol(argv[1], NULL, 10);

        if(characters_per_thread == 0){
            characters_per_thread = CHARACTERS_PER_THREAD;
        }

        if( is_directory(argv[1], true) ){
            path = argv[1];
        }

    }

    if( argc == 3 ){

        path = argv[1];
        if( !is_directory(path, false) ){
            fprintf(stderr, "Invalid directory path specified -- Exiting with failure\n");
            exit(EXIT_FAILURE);
        }

        characters_per_thread = strtol(argv[2], NULL, 10);
        if(characters_per_thread == 0){
            fprintf(stderr, "Invalid number of characters per thread: %d\n", argv[2]);
            fprintf(stderr, "Default value of 50 will be used\n");
            characters_per_thread = CHARACTERS_PER_THREAD;
        }
    }
//    printf("Path: %s %s directory\n", path, is_directory(path, false) == true ? "is a" : "is not a");
//    printf("Number of chars per thread: %ld\n", characters_per_thread);


    DIR* d;
    struct dirent* dir;
    d = opendir(path);
    char* filepath = (char*)calloc(MAX_PATH_LENGTH, sizeof(char));
    if(d){
        while( (dir = readdir(d)) != NULL ){
            if(dir->d_type == DT_REG){
                strcpy(filepath, path);
                strncat(filepath, "/", 2);
                strncat(filepath, dir->d_name, sizeof(dir->d_name));
                if( !can_open(filepath) ) continue;
                if(strcmp(filepath, "./output.txt") == 0)continue;
                if(is_ascii_file(filepath)){
                    pid_t pid;
                    argv[1] = filepath;
                    argv[2] = ltoa(characters_per_thread);
                    pid = fork();
                    if(pid == -1){
                        fprintf(stderr, "func: %s file: %s line: %d errno: %d\n", __func__, __FILE__, __LINE__, errno);
                        perror("perror ");
                    }else if(pid == 0){
                        execv("word_counter", argv);
                        exit(EXIT_FAILURE);
                    }else{
                        continue;
                    }
                }
                memset(filepath, 0, sizeof(filepath));
            }
        }
        wait(NULL);
        // waitpid(-1, NULL, WNOHANG);
        closedir(d);
    }
    /* ### ### End of driver program ### ###*/
    return 0;
}
/*Function that returns true if the path specified corresponds to a directory*/
bool is_directory(char* path, bool suppress_error_messages){
    struct stat stat_buffer;
    if (stat(path, &stat_buffer) != 0) {
        if(!suppress_error_messages) {
            fprintf(stderr, "func: %s file: %s line: %d errno: %d\n", __func__, __FILE__, __LINE__, errno);
            perror("perror ");
        }
        return false;
    }
    return S_ISDIR(stat_buffer.st_mode);
}
/*Function that returns true if the file specified in the path is a regular file and not a FIFO pipe for example.*/
bool is_file(char *path)
{
    struct stat stat_buffer;
    if( stat(path, &stat_buffer) != 0 ){
        fprintf(stderr, "func: %s file: %s line: %d errno: %d\n", __func__, __FILE__, __LINE__, errno);
        perror("perror ");
        return false;//error checking message
    }
    return S_ISREG(stat_buffer.st_mode);
}
/*Function that changes the working directory to the path specified.*/
bool change_directory(char* path){
    if( chdir(path) != 0 ){
        fprintf(stderr, "func: %s file: %s line: %d errno: %d\n", __func__, __FILE__, __LINE__, errno);
        perror("perror: ");
        return false;
    }
    return true;
}
/*Function that returns true if the file can be opened and hence its file descriptor can be obtained and it is greater than -1.*/
bool can_open(char* filepath){
    ssize_t fd = open(filepath, O_RDONLY);
    if(fd >= 0 ){
        close(fd);
        return true;
    }
    return false;
}
/*
    Function that returns true if the file contains only ASCII characters and hence it's an ASCII file or false otherwise.
*/
bool is_ascii_file(char* filepath){
    int fd = open(filepath, O_RDONLY);
    if( fd < 0 ){
        fprintf(stderr, "func: %s file: %s line: %d errno: %d\n", __func__, __FILE__, __LINE__, errno);
        perror("perror: ");
        return false;
    }
    char* buffer = (char*)calloc(2, sizeof(char));
    ssize_t bytes_read;
    while( (bytes_read = read(fd, buffer,1)) > 0){
        int asciiCharacterDecimalValue = (int)buffer[0];
        if(asciiCharacterDecimalValue < 0 || asciiCharacterDecimalValue > 255) {
            close(fd);
            return false;
        }
    }
    close(fd);
    return true;
}
/*
    Function that converts a long value to its string representation.
    The max number of characters for the llong number is 128.
*/
char* ltoa(long value){
    char* buffer = (char*)calloc(128, sizeof(char));
    snprintf(buffer, 128, "%ld", value);
    return buffer;
}

/*Signal handler that simply prints an informative message to the user*/
void signal_handler(int sig){
    printf("Signal %d caught. Continuing execution...\n", signal);
}


