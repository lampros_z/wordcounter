#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <assert.h>
#include "vector.h"
#define INITIAL_CAPACITY 2
#define true 1
#define false 0
#define null NULL


struct _vector
{
    value_type* array;
    size_t size;
    size_t capacity;
};
/*
    Function that allocates memory for a vector instance
*/
vector vector_create(){
    vector v = (vector) malloc(sizeof(struct _vector));
    if(v == null){
        fprintf(stderr, "memmory err: Not enough memory\n");
        abort();
    }
    v -> size = 0;
    v -> capacity = INITIAL_CAPACITY;
    v -> array = (value_type *)malloc(sizeof(value_type) * INITIAL_CAPACITY);
    if(v -> array == null){
        fprintf(stderr, "memmory err: Not enough memory\n");
        abort();
    }
    return v;
}
/*
    Function that de-allocates the memory used by the vector
*/
void vector_destroy(vector v){
    assert(v);
    free(v -> array);
    free(v);
}

value_type vector_get(vector v, int position){
    assert(v);
    if(position < 0 || position >= v -> size){
        fprintf(stderr, "index err: Out of index\n");
        abort();
    }
    return v -> array[position];
}
/*
    Function that updates an element at the specified position
*/
void vector_put(vector v, int position, value_type value){
    assert(v);
    assert(value);
    if(position < 0 || position >= v -> size){
        fprintf(stderr, "index err: Out of index\n");
        abort();
    }
    v -> array[position] = value;
}
/*  Private(scope only inside this file) helper method used for increasing the capacity of the vector
    In other words it resizes the vector increasing its size.
*/
static
void vector_double_capacity(vector v){
    assert(v);
    // printf("%s : %d\n", "capacity before", v -> capacity);
    v -> capacity *= 2;
    v -> array = realloc(v->array, v->capacity);
    if(v -> array == null){
        fprintf(stderr, "memmory err: Not enough memory\n");
        free(v -> array);
        free(v);
        abort();
    }
    // printf("%s : %d\n", "capacity after", v -> capacity);
}
/*  Private(scope only inside this file) helper method used for reducing the capacity of the vector.
    In other words it resizes the vector decreasing its size.
*/
static
void vector_reduce_capacity(vector v){
    assert(v);
    v -> capacity /= 2;
    v -> array = realloc(v->array, v->capacity);
    if(v -> array == null){
        fprintf(stderr, "memmory err: Unable to shrink memory block\n");
        free(v -> array);
        free(v);
        abort();
    }
}

/*Function that just adds an element at the end of the given vector*/
void vector_add(vector v, value_type value){
    assert(v);
    if( (v -> size) >= (v -> capacity)){
        //Double capacity
        vector_double_capacity(v);
    }
    v -> array[v->size] = value;
    v -> size += 1;
}
/*Function that adds an element at the given position*/
void vector_add_at(vector v, int position, value_type value){
    assert(v);
    if( (v -> size) >= (v -> capacity)){
        //Double capacity
        vector_double_capacity(v);
    }
    if(position < 0 || position >= v -> size){
        fprintf(stderr, "index err: Out of index\n");
        abort();
    }
   
    //[10 20 30 40 ... ]
    // [10 50 20 30 40  ]
    //  0  1  2  3  4
    size_t index = 0;
    size_t next_index = 0;
    value_type current_element = v -> array[position];
    value_type next_element;
    //shift right all elements
    for( index = position; index < v -> size; index++ ){
        //calculate next index
        next_index = index + 1;
        //store next element
        next_element = v -> array[next_index];
        //shift curent element to next index
        v -> array[next_index] = current_element;
        //load the saved next element as the current element
        current_element = next_element;
    }
    //add new value at specified position
    v -> array[position] = value;
    //increase size
    v -> size += 1;
    
}
/*Function that removes an element at the given position*/
value_type vector_remove_at(vector v, int position){
    assert(v);
    if(position < 0 || position >= v -> size){
        fprintf(stderr, "index err: Out of index\n");
        abort();
    }
    value_type removed_value = v -> array[position];
    // [10 50 20 30 40  ]
    //[10 20 30 40  ...]
    //  0  1  2  3  4
    size_t index = 0;
    size_t previous_index = 0;
    size_t last_index = v -> size -1;
    value_type current_element = v -> array[last_index];
    value_type previous_element;
    for( index = last_index; index > position; index--){
        previous_index = index - 1;
        previous_element = v -> array[previous_index];
        v -> array[previous_index] = current_element;
        current_element = previous_element;
    }
    v -> size -= 1;
    if( v -> size / (double)v-> capacity <= 0.25 ){
        vector_reduce_capacity(v);
    }
    return removed_value;
}
/*Function that removes all the elements of the vector*/
void vector_clear(vector v){
    free(v -> array);
    v -> size = 0;
    v -> capacity = INITIAL_CAPACITY;
    v -> array = (value_type *)malloc(sizeof(value_type) * INITIAL_CAPACITY);
    if(v -> array == null){
        fprintf(stderr, "memmory err: Not enough memory\n");
        abort();
    }
}
/*Function that prints all the elements in the vector*/
void print_vector(vector v){
    size_t length = v -> size;
    fprintf(stdout, "[");
    for(size_t index = 0; index < length; index++)
        fprintf(stdout, " %c", v->array[index]);
    fprintf(stdout, " ]\n");
}
/*
    It returns true value(non-zero) if the vector is empty or zero otherwise
*/
int vector_is_empty(vector v){
    return v -> size == 0;
}
/*
    Returns the size of the vector. In other words, it returns
    the number of elements inserted.
*/
size_t vector_size(vector v){
    return v -> size;
}
