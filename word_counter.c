
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <string.h>
#include "vector.h"
/*To enable debugging set value to 1 else 0*/
#define DEBUG_MODE_ON 1
/*Macros for ASCII Dec values*/
#define ASCII_MIN 0
#define ASCII_TAB 9
#define ASCII_LF 10
#define ASCII_SPACE 32
#define ASCII_COMMA 44
#define ASCII_SLASH 47
#define ASCII_COLON 58
#define ASCCII_MAX 127
/*Path of the report file where results will be saved*/
#define OUTPUT_FILE "output.txt"
/*Number of worker threads to use for calculating words in the file*/
#define MAX_NUM_THREADS 4
/*Words found*/
int words = 0;
/*Size of file*/
off_t file_size;
/*File to search for word occurences*/
char* filepath;
/*Initialize mutex*/
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
/* ### Function declarations ###*/
void* Pthr_word_count(void* payload);
off_t get_file_size(char* filepath);
char* load_file(char* filepath);
/**
* Helper struct used at thread function.
* It is used for parallelizing the file processing. Specifically, it contains all the 
* necessary information like start and end offset of the file for the specific thread
*   with rank n and a pointer to the contents of the file itself.
*/
struct thread_argument{
    /*Rank of the thread*/
    long rank;
    /*Start offset of file for this thread*/
    off_t start;
    /*End offset of file for this thread*/
    off_t end;
    /*Pointer to the memory location of the file data*/
    char* file_content;
};

typedef struct thread_argument thread_arg;
typedef struct thread_argument* thread_argptr;

/*Function decl*/
void threadArgs_init(thread_argptr threadArgs, char *file);

int main(int argc, char **argv){
    /*Get current proccess id*/
    pid_t pid = getpid();
    /*Set filepath global variable*/
    filepath = argv[1];
    /*Ask system for file size at the specified path*/
    file_size = get_file_size(filepath);
    /*Thread counter variable*/
    long thread;
    /* ### Initialize thread handles ###*/
    pthread_t* thread_handles = (pthread_t*)calloc(MAX_NUM_THREADS, sizeof(pthread_t));

    /* ### LOAD FILE ###*/
    char* file = load_file(filepath);

    /* ###Parallelize -> Calculate thread work ### */
    thread_argptr threadArgs = (thread_argptr)calloc(MAX_NUM_THREADS, sizeof(thread_arg));
    threadArgs_init(threadArgs, file);
    
    
    if(DEBUG_MODE_ON)
        printf("filesize: %ld\n", file_size);
    /*Spwan workers*/
    for ( thread = 0; thread < MAX_NUM_THREADS; ++thread) {
        
        if(DEBUG_MODE_ON){
            printf("start: %ld\n", threadArgs[thread].start);
            printf("end: %ld\n", threadArgs[thread].end);
        }

        pthread_create(&thread_handles[thread], NULL, Pthr_word_count, (thread_argptr)&threadArgs[thread]);
    }
    /*Wait all workers to finish their task*/
    for (thread = 0; thread < MAX_NUM_THREADS; ++thread) {
        pthread_join(thread_handles[thread], NULL);
    }
    /*Clear mutex from system*/
    pthread_mutex_destroy(&mutex);

    /*Trim first two symbol of path (./)*/
    char buffer[512];
    if(filepath[0] == '.' && filepath[1] == '/'){
        for (int i = 0; i < strlen(filepath) - 1; ++i) {
            filepath[i] = filepath[i+2];
            filepath[i+1] = filepath[i+2+1];
        }
    }
   int fd = -1;
   if(DEBUG_MODE_ON != 1){
        fd = open(OUTPUT_FILE,O_CREAT | O_WRONLY | O_APPEND, 0700);
        if(fd < 0){
            perror("open ");
            exit(EXIT_FAILURE);
        }    
   }
   /*Create formated text using safe snprintf function*/
    snprintf(buffer, 512, "%ld,%s,%d\n", pid, filepath, words);
    if(DEBUG_MODE_ON)
        printf("%s\n", buffer);
   /*Append results to file*/
   if(DEBUG_MODE_ON != 1){
       if( write(fd, buffer, strlen(buffer)) != strlen(buffer)){
           perror("write");
        }
     if( close(fd) != 0 ){
         perror("close ");
         exit(EXIT_FAILURE);
     }
   }
}

/**
* Thread function. This function calculates the total number of words contained in an ASCII file.
* The processing of the file as well as the counting happens concurently.
* @param paylaod a pointer to struct thread_argument which contains all the necessary information to parallelize the task of word counting.
*/
void* Pthr_word_count(void* payload){
    thread_argptr thread_argument = (thread_argptr)payload;

    int local_words = 0;
    int asciiChar;
    vector vector_stack = vector_create();
    int eof = 0;
    for (int i = thread_argument -> start; i <= thread_argument -> end ; ++i) {
        if(i >= file_size){
            eof = 1;
            break;
        }
        asciiChar = thread_argument -> file_content[i];

        if( asciiChar == ASCII_TAB || asciiChar == ASCII_LF 
            || asciiChar == ASCII_SPACE || asciiChar == ASCII_COMMA 
            || asciiChar == ASCII_SLASH || asciiChar == ASCII_COLON){

            int atLeastOneAlphanumeric = 0;
            for (int j = 0; j < vector_size(vector_stack); ++j) {
                char character = vector_get(vector_stack, j);
                if(isalnum(character)) {
                    atLeastOneAlphanumeric = 1;
                    break;
                }
            }
            if(!vector_is_empty(vector_stack) && atLeastOneAlphanumeric){
                ++local_words;
                vector_clear(vector_stack);
            }
        }

        if( asciiChar > ASCII_MIN && asciiChar < ASCCII_MAX )
            vector_add(vector_stack, asciiChar);

    }
    int atLeastOneAlphanumeric = 0;
    for (int j = 0; j < vector_size(vector_stack); ++j) {
        char character = vector_get(vector_stack, j);
        if(isalnum(character)) {
            atLeastOneAlphanumeric = 1;
            break;
        }
    }
    if(!vector_is_empty(vector_stack) && atLeastOneAlphanumeric && eof){
        ++local_words;
        vector_clear(vector_stack);
    }
    pthread_mutex_lock(&mutex);
    words += local_words;
    pthread_mutex_unlock(&mutex);
    return NULL;
}
/**
* Given a path(URI) pointing to a file find and return its size. 
* @param filepath String representation of a file path pointing to a file in disk
* @return The size of the file as an unsigned long number
*/
off_t get_file_size(char* filepath){
    struct stat file_metadata;
    int fd = open(filepath, O_RDONLY);
    if(fd < 0){
        perror("open ");
        return 0lu;
    }
    if( fstat(fd, &file_metadata) != 0 ){
        perror("fstat ");
        close(fd);
        return 0lu;
    }
    close(fd);
    return file_metadata.st_size;
}
/**
* Helper function responsible for dividing the file into chunks which will be processed later
* by the worker threads. In other words we could say that this function makes all the necessary preprocessing required
* to help parallelize a task.
* @param threadArgs A pointer to a struct containing all the necessary information for parallelizing work to theads
* @param file File path(URI) pointing to the file which will be used for parallel processing
*/
void threadArgs_init(thread_argptr threadArgs, char *file){
    off_t offset = file_size / MAX_NUM_THREADS;
    off_t start;
    off_t end;
    for (int i = 0; i < MAX_NUM_THREADS; ++i) {

        if(i == 0)
            start = i * offset;
        else
            start = threadArgs[i-1].end+1;

        threadArgs[i].start = start;

        end = start + offset;

        if(i == MAX_NUM_THREADS - 1){
            while( end > file_size)--end;
        } else {
            while ( file[end] != ASCII_TAB && file[end] != ASCII_LF 
                    && file[end] != ASCII_SPACE && file[end] != ASCII_COMMA 
                    && file[end] != ASCII_SLASH && file[end] != ASCII_COLON 
                    && end < file_size
                    ) end++;
        }
        if(start == file_size)
            threadArgs[i].start = file_size+1;

        if(start > end)
            end = start;

        if(end > file_size)
            end = file_size+1;
        threadArgs[i].end=end;
        threadArgs[i].rank=i;
        threadArgs[i].file_content = file;
    }
}
/**
* A function that loads an ASCII text file from disk to memory.
* @param filepath File path(URI) of the file
* @return Data of the loaded file stored in a string buffer
*/
char* load_file(char* filepath){
    /* ### LOAD FILE ### */
    int fd = open(filepath, O_RDONLY);
    if(fd < 0){
        perror("open ");
        exit(EXIT_FAILURE);
    }
    char* file = (char *)calloc(file_size+1, sizeof(char));
    if(read(fd, file, file_size) != file_size){
        perror("read ");
        exit(EXIT_FAILURE);
    }
    if( close(fd) != 0 )
    {
        perror("close ");
        exit(EXIT_FAILURE);
    }
    return file;
}